﻿/*
Author: Stefan Stegic
Date: 27.8.2019.

Description:
- Main birdoid simulation controller.
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class SimulationController : MonoBehaviour
{
	[SerializeField]
	private Boid prefab;
	[SerializeField]
	private int boidCount;
	
	private float Xmin, Xmax, Ymin, Ymax;
	
	[Header("Boid movement parameters")]
	[SerializeField]
	private float minSpeed;
	[SerializeField]
	private float maxSpeed;
	[SerializeField]
	private float maxAcceleration;
	private float sqrMaxAcceleration;
	[SerializeField]
	private float maxRotSpeed;
	[SerializeField]
	private float separationDistance;
	[SerializeField]
	private float startleSpeed;
	[SerializeField]
	private bool tendToTarget;

	[Header("Weights")]
	[SerializeField]
	private float cohesionWeight;
	private float cohesionMul;
	[SerializeField]
	private float alignmentWeight;
	[SerializeField]
	private float separationWeight;
	[SerializeField]
	private float boundingWeight;
	[SerializeField]
	private float targetWeight;

	[Header("Drawing")]
	[SerializeField]
	private bool drawCenterOfMass;
	[SerializeField]
	private bool drawAverageAlignment;
	[SerializeField]
	private bool drawTargetPosition;
	

	private List<Boid> boids;

	private Vector2 v1, v2, v3, v4, v5;
	private Quaternion targetRot;

	[Header("Info")]
	[SerializeField]
	private Vector2 avgAlignment;
	[SerializeField]
	private Vector2 targetPosition;
	[SerializeField]
	private Vector2 centerOfMass;

	private delegate Vector2 BoidDelegate(int i);
	private List<BoidDelegate> accelerationMethods;

	private void Start() {
		// Component priority
		accelerationMethods = new List<BoidDelegate>();
		accelerationMethods.Add(SeparationVector);
		accelerationMethods.Add(CohesionVector);
		accelerationMethods.Add(AlignmentVector);
		accelerationMethods.Add(BoundingVector);
		accelerationMethods.Add(TargetVector);

		// Calculate variables
		cohesionMul = cohesionWeight;

		// Get bounds
		BoxCollider2D col = GetComponent<BoxCollider2D>();
		Xmin = col.bounds.min.x;
		Xmax = col.bounds.max.x;
		Ymin = col.bounds.min.y;
		Ymax = col.bounds.max.y;

		// Initialization
		boids = new List<Boid>();
		Boid instance;
		for(int i=0; i < boidCount; i++)
		{
			instance = Instantiate(prefab, (Vector2) Random.onUnitSphere * Random.Range(30, 60), Quaternion.identity);
			boids.Add(instance);
		}
	}

	private void FindCenterOfMass()
	{
		// Calculate average flock position
		centerOfMass = Vector2.zero;
		for(int i=0; i < boids.Count; i++)
		{
			centerOfMass += (Vector2) boids[i].transform.position;
		}
		centerOfMass /= boids.Count-1;
	}

	private void FindAvgAlignment()
	{
		// Calculate average flock direction
		avgAlignment = Vector2.zero;
		for(int i=0; i < boids.Count; i++)
		{
			avgAlignment += boids[i].rb.velocity;
		}
		avgAlignment /= boids.Count-1;
	}

	private Vector2 CohesionVector(int index)
	{
		// Finds cohesion velocity factor for a given boid index
		Vector2 avg = centerOfMass - (Vector2)boids[index].transform.position/(boids.Count-1); // Take me out of the equation
		return (avg - (Vector2) boids[index].transform.position) * cohesionMul;
		
	}

	private Vector2 AlignmentVector(int index)
	{
		// Finds alignment velocity factor for a given boid index

		Vector2 avg = avgAlignment - boids[index].rb.velocity/(boids.Count-1); // Take me out of the equation
		return (avg - boids[index].rb.velocity) * alignmentWeight;
	}

	private Vector2 SeparationVector(int index)
	{
		// Finds separation velocity factor for a given boid index

		Vector2 sep = Vector2.zero;
		Vector2 distance;
		for(int i=0; i < boids.Count; i++)
		{
			if(i == index) continue; // Skip the one that it's being calculated for
			distance = boids[index].transform.position - boids[i].transform.position;
			if(distance.magnitude < separationDistance)
			{
				sep -= (Vector2) (boids[i].transform.position - boids[index].transform.position);
			}
		}

		return sep * separationWeight;

	}

	private Vector2 BoundingVector(int index)
	{
		Vector2 pos = boids[index].transform.position;
		Vector2 v = Vector2.zero;
		if(pos.x < Xmin)
		{
			v -= Vector2.right * (pos.x - Xmin);
		}
		if(pos.x > Xmax)
		{
			v += Vector2.left * (pos.x - Xmax);
		}
		if(pos.y < Ymin)
		{
			v -= Vector2.up * (pos.y - Ymin);
		}
		if(pos.y > Ymax)
		{
			v += Vector2.down * (pos.y - Ymax);
		}
		
		return v * boundingWeight;
	}

	private Vector2 TargetVector(int index)
	{
		if(!tendToTarget) return Vector2.zero;
		return (targetPosition - (Vector2) boids[index].transform.position) * targetWeight;
	}

	private void Update() {
		if(Input.GetKeyDown(KeyCode.S))
		{
			cohesionMul = -startleSpeed;
		}

		if(Input.GetKeyUp(KeyCode.S))
		{
			cohesionMul = cohesionWeight;
		}

		if(Input.GetKeyDown(KeyCode.UpArrow))
		{
			boidCount++;
		}

		if(Input.GetKeyDown(KeyCode.DownArrow))
		{
			boidCount--;
		}

		if(Input.GetMouseButton(0))
		{
			Vector3 mousePoz = Input.mousePosition;
			mousePoz.z = 25;
			targetPosition = Camera.main.ScreenToWorldPoint(mousePoz);
			tendToTarget = true;
		}

		if(Input.GetMouseButtonUp(0))
		{
			tendToTarget = false;
		}
	}

	private void FixedUpdate() {
		// Keep boid count consistent with parameter
		while(boidCount > boids.Count)
		{
			Boid instance = Instantiate(prefab, (Vector2) Random.onUnitSphere * Random.Range(30, 60), Quaternion.identity);
			boids.Add(instance);
		}

		while(boidCount < boids.Count)
		{
			Destroy(boids[boids.Count-1].gameObject);
			boids.RemoveAt(boids.Count-1);
		}

		// Boid flock physics frame

		// No need to recalculate these for each boid, they are 
		// constant for one frame
		FindCenterOfMass();
		FindAvgAlignment();

		// Apply rules to each boid with respect to priority
		// --> Keep adding until max is exceeded, then cut
		Vector2 a;
		int methodIndex;
		sqrMaxAcceleration = maxAcceleration * maxAcceleration;
		for(int i = 0; i < boids.Count; i++)
		{
			a = Vector2.zero;
			
			methodIndex = 0;
			while(true)
			{
				// Process current acceleration component and add
				a += accelerationMethods[methodIndex](i);

				// If it's too much, break
				if(a.sqrMagnitude > sqrMaxAcceleration)
				{
					a = a.normalized * maxAcceleration;
					break;
				}

				// Increment counter
				methodIndex++;

				// If we've processed all of them, break
				if(methodIndex >= accelerationMethods.Count)
				{
					break;
				}

			}
			
			// Add to boid velocity
			boids[i].rb.velocity += a;

			// Assign a minimum speed
			if(boids[i].rb.velocity.magnitude < minSpeed) boids[i].rb.velocity = boids[i].rb.velocity.normalized * minSpeed;

			// Clamp to max speed
			if(boids[i].rb.velocity.magnitude > maxSpeed) boids[i].rb.velocity = boids[i].rb.velocity.normalized * maxSpeed;
			
			// Rotate slowly to match velocity vector
			targetRot = Quaternion.LookRotation(Vector3.forward, boids[i].rb.velocity);
			boids[i].transform.rotation = Quaternion.Lerp(boids[i].transform.rotation, targetRot, Time.deltaTime * maxRotSpeed);
		}
	}

	private void OnDrawGizmos() {
		if(drawAverageAlignment)
		{
			Gizmos.color = Color.red;
			Gizmos.DrawLine(centerOfMass, centerOfMass + avgAlignment.normalized*5);
		}
		if(drawCenterOfMass)
		{
			Gizmos.color = Color.red;
			Gizmos.DrawSphere(centerOfMass, 0.5f);
		}

		if(drawTargetPosition && tendToTarget)
		{
			Gizmos.color = Color.white;
			Gizmos.DrawCube(targetPosition, Vector3.one);
		}

	}
}
