﻿/*
Author: Stefan Stegic
Date: 27.8.2019.

Description:
- Control of birdoid behaviour lies in the simulation controller class.
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boid : MonoBehaviour
{
	public Rigidbody2D rb;

	private void Awake() {
		rb = GetComponent<Rigidbody2D>();
		//rb.velocity = Random.onUnitSphere * 20;

		//GetComponent<SpriteRenderer>().color = Random.ColorHSV();
	}
}
